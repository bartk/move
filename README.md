[![](http://www.r-pkg.org/badges/version/move)](http://cran.rstudio.com/web/packages/move/index.html)
[![cran checks](https://badges.cranchecks.info/worst/move.svg)](https://cran.r-project.org/web/checks/check_results_move.html)
[![](http://cranlogs.r-pkg.org/badges/move)](http://cran.rstudio.com/web/packages/move/index.html)
[![build status](https://gitlab.com/bartk/move/badges/master/pipeline.svg)](https://gitlab.com/bartk/move/pipelines)
[![coverage status](https://gitlab.com/bartk/move/badges/master/coverage.svg)](http://bartk.gitlab.io/move/)

## Installation instructions

### Stable release
The package is available on [CRAN](https://cran.r-project.org/package=move). for more information see the  [website](https://bartk.gitlab.io/move/). For installing run
```R
install.packages('move')
```

### Install the development version
``` R
require('devtools')
install_git('https://gitlab.com/bartk/move.git')
```

## Bugs and Feature requests

Bug reports and feature requests are very welcome and can be uploaded to the gitlab issue tracker (https://gitlab.com/bartk/move/issues)
